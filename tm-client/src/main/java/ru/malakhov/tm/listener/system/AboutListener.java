package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.event.RunEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    private void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Sergei Malakhov");
        System.out.println("smalakhov2@rencredit.ru");
    }

    @Override
    @EventListener(condition = "@aboutListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        execute();
    }

    @EventListener(condition = "@aboutListener.arg() == #event.name")
    public void handler(@NotNull final RunEvent event) {
        execute();
    }

    @Override
    public boolean secure() {
        return false;
    }

}
