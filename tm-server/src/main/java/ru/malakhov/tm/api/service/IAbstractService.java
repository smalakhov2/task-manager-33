package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.AbstractEntityDto;

import java.util.Collection;

public interface IAbstractService<E extends AbstractEntityDto> {

    void persist(@Nullable Collection entities);

    void persist(E entity);
}
