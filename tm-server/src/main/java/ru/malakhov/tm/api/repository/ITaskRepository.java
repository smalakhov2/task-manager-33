package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskDto> {

    @NotNull
    List<TaskDto> findAllDto();

    @NotNull
    List<Task> findAllEntity();

    @NotNull
    List<TaskDto> findAllDtoByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllEntityByUserId(@NotNull String userId);

    @Nullable
    TaskDto findOneDtoById(@NotNull String id);

    @Nullable
    Task findOneEntityById(@NotNull String id);

    @Nullable
    TaskDto findOneDtoById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneEntityById(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDto findOneDtoByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneEntityByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDto findOneDtoByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task findOneEntityByName(@NotNull String userId, @NotNull String name);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    void removeOneById(@NotNull String id);

    void removeOneById(@NotNull String userId, @NotNull String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @NotNull String name);

}