package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public final class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User ID is empty...");
    }

}
