package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public final class EmptyTaskException extends AbstractException {

    public EmptyTaskException() {
        super("Error! Task is null...");
    }

}
