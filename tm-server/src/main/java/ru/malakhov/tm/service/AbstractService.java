package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.dto.AbstractEntityDto;

import java.util.Collection;
import java.util.Objects;

public abstract class AbstractService<E extends AbstractEntityDto, R extends IRepository<E>> implements IService<E, R> {

    @Autowired
    protected R repository;

    protected AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public void persist(@Nullable final E entity) {
        if (entity == null) return;
        repository.persist(entity);
    }

    @Override
    @Transactional
    public void persist(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        entities.removeIf(Objects::isNull);
        repository.persist(entities);
    }

    @Override
    @SafeVarargs
    @Transactional
    public final void persist(@Nullable final E... entities) {
        if (entities == null) return;
        repository.persist(entities);
    }

    @Override
    @Transactional
    public void merge(@Nullable final E entity) {
        if (entity == null) return;
        repository.merge(entity);
    }

    @Override
    @Transactional
    public void merge(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        entities.removeIf(Objects::isNull);
        repository.merge(entities);
    }

    @Override
    @SafeVarargs
    @Transactional
    public final void merge(@Nullable final E... entities) {
        if (entities == null) return;
        repository.merge(entities);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable E entity) {
        if (entity == null) return;
        repository.removeOne(entity);
    }

}
