package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.entity.Session;

import java.util.List;

@Repository
public class SessionRepository extends AbstractRepository<SessionDto> implements ISessionRepository {

    @NotNull
    @Override
    public List<SessionDto> findAllDto() {
        return entityManager.createQuery("SELECT e FROM SessionDto e", SessionDto.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAllEntity() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<SessionDto> findAllDtoByUserId(@NotNull final String userId) {
        @NotNull final List<SessionDto> sessions =
                entityManager.createQuery("SELECT e FROM SessionDto e WHERE e.userId=:userId", SessionDto.class)
                        .setParameter("userId", userId)
                        .getResultList();
        return sessions;
    }

    @NotNull
    @Override
    public List<Session> findAllEntityByUserId(@NotNull final String userId) {
        @NotNull final List<Session> sessions =
                entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id=:userId", Session.class)
                        .setParameter("userId", userId)
                        .getResultList();
        return sessions;
    }

    @Nullable
    @Override
    public SessionDto findOneDtoById(@NotNull final String id) {
        @NotNull final List<SessionDto> sessions =
                entityManager.createQuery("SELECT e FROM SessionDto e WHERE e.id=:id", SessionDto.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (sessions.isEmpty()) return null;
        return sessions.get(0);
    }

    @Nullable
    @Override
    public Session findOneEntityById(@NotNull final String id) {
        @NotNull final List<Session> sessions =
                entityManager.createQuery("SELECT e FROM Session e WHERE e.id=:id", Session.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (sessions.isEmpty()) return null;
        return sessions.get(0);
    }

    @Override
    public void removeAll() {
        @NotNull final List<Session> sessions = findAllEntity();
        for (@NotNull final Session session : sessions) entityManager.remove(session);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Session> sessions = findAllEntityByUserId(userId);
        for (@NotNull final Session session : sessions) entityManager.remove(session);
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final Session session = findOneEntityById(id);
        if (session == null) return;
        entityManager.remove(session);
    }

}